<?php
session_start();

$id = 0;
$nombre= "";
$apellido="";
$sexo="";
$edad="";
$fecha="";
$update = false;

if
    (isset($_POST['guardar'])){
    $mongo= new Mongo();
    $db= $mongo->selectDB("muziek");
    $c_alumnos=$mongo->selectCollection("muziek","Alumnos");

    $nombre = $_POST['nombre_a'];
    $apellido = $_POST['apellido_a'];
    $sexo = $_POST['sexo'];
    $edad = $_POST['edad'];
    $fecha = $_POST['fecha'];
    
    $nuevoAlumno = array(
        'nombre' =>$nombre , 
        'apellido' => $apellido, 
        'sexo' => $sexo, 
        'edad' => $edad, 
        'fechaNacimiento' => $fecha);
    $c_alumnos->insert($nuevoAlumno);
    $_SESSION['mensaje'] = "Registro Agregado";
    header('location: CrudAlumnos.php');
} 

if
    (isset($_POST['actualizar'])){
   $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_alumnos = $mongo -> selectCollection("muziek","Alumnos");
        $nombre=$_POST['nombre_a'];
        $apellido=$_POST['apellido_a'];
        $sexo=$_POST['sexo'];
        $edad=$_POST['edad'];
        $fecha=$_POST['fecha'];
        $id=$_POST['id'];
        $condicion = array('_id' => new MongoID($id));
        $modi = array('nombre' => $nombre,
            'apellido'=>$apellido,
            'sexo'=>$sexo,
            'edad'=>$edad,
            'fechaNacimiento'=>$fecha
             
         );
        $c_alumnos->update($condicion, $modi);

    $_SESSION['mensaje'] = "Registro Actualizado";
    header('location: CrudAlumnos.php');
}
?>