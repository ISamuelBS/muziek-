<?php include('php_code_alumnos.php');?>
<?php 
if (isset($_GET['editarC'])) {
    $id_cliente = $_GET['editarC'];
    $update = true;
    $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_alumnos = $mongo -> selectCollection("muziek","Alumnos");
    $id = $_GET['editarC'];
    $condicion = array('_id' => new MongoId($id));
    if($c_alumnos->count($condicion)==1){

        $row=$c_alumnos->findOne($condicion);
        $nombre = $row['nombre'];
        $apellido = $row['apellido'];
        $sexo = $row['sexo'];
        $edad = $row['edad'];
        $fecha = $row['fechaNacimiento'];
        $_SESSION['mensaje'] = "Editando alumno";
    }
} 
if(isset($_GET['eliminar'])){
   $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_alumnos = $mongo -> selectCollection("muziek","Alumnos");
        $id = $_GET['eliminar'];
        $condicion = array('_id' => new MongoId($id));
        $c_alumnos->remove($condicion);
        $_SESSION['mensaje']="Registro eliminado";
}

 ?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="../images/Muziek-LOGO.ico" sizes="16x16 24x24 36x36 48x48">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300">
    <link rel="stylesheet" href="../css/font.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>CherryCode: Muziek</title>
    <style>
  .select
  {
  border: 1px solid #FFFFFF;
  font-size: 18px;
  font-family: Arial, Verdana;
  padding-left: 7px;
  padding-right: 7px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  -o-border-radius: 4px;
  background: #FFFFFF;
  background: linear-gradient(left, #FFFFFF, #F7F9FA);
  background: -moz-linear-gradient(left, #FFFFFF, #F7F9FA);
  background: -webkit-linear-gradient(left, #FFFFFF, #F7F9FA);
  background: -o-linear-gradient(left, #FFFFFF, #F7F9FA);
  color: #2E3133;
  }
  
  .select:hover
  {
  border-color: #FFFFFF;
  }
  
  .select option
  {
  border: 1px solid #FFFFFF;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  -o-border-radius: 4px;
  }
  
  .select option:hover
  {
  background: #FC4F06;
  background: linear-gradient(left, #FC4F06, #D85F2B);
  background: -moz-linear-gradient(left, #FC4F06, #D85F2B);
  background: -webkit-linear-gradient(left, #FC4F06, #D85F2B);
  background: -o-linear-gradient(left, #FC4F06, #D85F2B);
  font-style: italic;
  color: #FFFFFF;
  }
 </style>
  </head>
  <body>

    <input type="checkbox" id="btn-nav" class="checkbox">
    <header>
      <div class="header-container">
        <img class="header-logo" src="../images/MuziekONE.png"> 
        <label for="btn-nav" class="btn-label">
          <div class="header-button"></div>
        </label>
      </div>
    </header>
    
    <nav class="menu">
      <ul>
       <li><a href="index.php"></a></li>
        <li><a href="CrudAlumnos.php">Alumnos</a></li>
        <li><a href="../cursos/CrudCursos.php">Cursos</a></li>
        <li><a href="../maestros/CrudMaestros.php">Maestros</a></li>
        <li><a href="../Nosotros/Nosotros.php">Acerca de Nosotros</a></li>
      </ul>           
    </nav>
    <br><br><br><br><br><br><br>
      <?php if(isset($_SESSION['mensaje'])){
    ?>
    <div class="mensaje">
        <?php
        $mensaje=$_SESSION['mensaje'];
        echo $mensaje;
    ?>
    </div>
    <?php } ?>
     <form method="post" action="CrudAlumnos.php">

        <input type="hidden" name="id" value="<?php echo$id?>">
        <div class="input-group">
            <label>Nombre Alumno</label>
            <input type="text" name="nombre_a" value="<?php echo$nombre?>" required>
        </div>
        <div class="input-group">
            <label>Apellidos</label>
          <input type="text" name="apellido_a" value="<?php echo$apellido?>" required>
        </div>
         <div class="input-group">
            <label>Sexo</label>
            <input type="text" name="sexo" value="<?php echo$sexo?>" required>
        </div>
          <div class="input-group">
           <label>Edad</label>
           <input type="text" name="edad" value="<?php echo$edad?>" required>
        <div class="input-group">
            <label>Fecha de Nacimiento</label>
            <input type="date" name="fecha" value="<?php echo$fecha?>">
        </div>
      
     <div class="input-group">
             <?php  if ($update == true): ?>
            <button class="btn" type="submit" name="actualizar">Actualizar</button>
            <?php else: ?>
            <button class="btn" type="submit" name="guardar">Registrar</button>
        <?php endif ?>
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <?php
      $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_alumnos = $mongo -> selectCollection("muziek","Alumnos");

    if($c_alumnos->count()==0){
        ?>
    <div class="vacio">Sin registros</div>
    <?php
    }else{
        ?>
    <table border="2">
        <thead>
            <tr>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Sexo</th>
            <th>Edad</th>
            <th>Fecha de Nacimiento</th>
            <th colspan="2">Acción</th>
            </tr>
        </thead>
        <?php
         $row=$c_alumnos->find();
        foreach ($row as $nombre) { ?>
        <tr>
            <td><?php echo $nombre['nombre'];?></td>
            <td><?php echo $nombre['apellido'];?></td>
            <td><?php echo $nombre['sexo'];?></td>
             <td><?php echo $nombre['edad'];?></td>
            <td><?php echo $nombre['fechaNacimiento'];?></td>
            <td><a href="CrudAlumnos.php?editarC=<?php echo $nombre['_id'];?>" class="edit_btn" >Modificar</a></td>
            <td><a href="CrudAlumnos.php?eliminar=<?php echo $nombre['_id'];?>" class="delete_btn">Eliminar</a></td>
        </tr>
        <?php }}?>
    </table>
    
    </form>
  </body>
</html>