<?php
session_start();
$id= 0;
$nombre_c = "";
$duracion = "";
$costo = "";
$dias = "";
$update = false;

if (isset($_POST['guardar'])) {
    $mongo= new Mongo();
    $db= $mongo->selectDB("muziek");
    $c_cursos=$mongo->selectCollection("muziek","Cursos");

    $id=$_POST["id"];
    $nombre_c=$_POST["nombre_c"];
    $costo=$_POST["costo"];
    $duracion=$_POST["duracion"];
    $dias=$_POST["dias"];

    $nuevoCurso= array(
        "nombre" => $nombre_c,
        "costo" => $costo,
        "duracion" => $duracion,
        "días" => str_split($dias, 4)
    );
    $c_cursos->insert($nuevoCurso);
    $_SESSION['mensaje'] = "Registro Agregado";
    header('location: CrudCursos.php');
}

if
   (isset($_POST['actualizar'])){
   $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_cursos = $mongo -> selectCollection("muziek","Cursos");
        $nombre_c=$_POST['nombre_c'];
        $costo=$_POST['costo'];
        $duracion=$_POST['duracion'];
        $dias=$_POST['dias'];
        $id=$_POST['id'];
        $condicion = array('_id' => new MongoID($id));
        $modi = array('nombre' => $nombre_c,
            'costo'=>$costo,
            'duracion'=>$duracion,
            'días'=> str_split($dias, 4)
             
         );
        $c_cursos->update($condicion, $modi);

    $_SESSION['mensaje'] = "Registro Actualizado";
    header('location: CrudCursos.php');
}

?>