<?php include('../alumnos/php_code_alumnos.php');?>
<?php 
if(isset($_GET['del'])){
$borrar = $_GET['del'];
$consulta = mysqli_query($db,"DELETE from clientes where id_cliente=$borrar");
 if (@count($consulta) == 1) {
        $_SESSION['mensaje'] = "Cliente Eliminado";
    }

}
 ?>


<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="../images/Muziek-LOGO.ico" sizes="16x16 24x24 36x36 48x48">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300">
    <link rel="stylesheet" href="../css/font.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>Listado de Cursos</title>
  </head>
  <body>
    <input type="checkbox" id="btn-nav" class="checkbox">
    <header>
      <div class="header-container">
        <img class="header-logo" src="../images/Muziek.png"> 
        <label for="btn-nav" class="btn-label">
          <div class="header-button"></div>
        </label>
      </div>
    </header>
    
    <nav class="menu">
       <ul>
       <li><a href="index.php"></a></li>
        <li><a href="../alumnos/CrudAlumnos.php">Alumnos</a></li>
        <li><a href="../cursos/CrudCursos.php">Cursos</a></li>
        <li><a href="../maestros/CrudMaestros.php">Maestros</a></li>
        <li><a href="../Nosotros/Nosotros.php">Acerca de Nosotros</a></li>
      </ul>                   
    </nav>
    <br><br><br><br><br><br><br>
    <input type="hidden" name="id" value="<?php echo $id_instrumento; ?>">
    <?php $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_cursos = $mongo -> selectCollection("muziek","Cursos");

    if($c_alumnos->count()==0){
        ?>
    <div class="vacio">Sin registros</div>
    <?php
    }else{
        ?>
    <table border="2">
        <thead>
            <tr>
            <th>Nombre</th>
            <th>Duración</th>
            <th>Costo</th>
            <th>Días</th>
            <th colspan="2">Acción</th>
            </tr>
        </thead>
        <?php  $row=$c_cursos->find();
        foreach ($row as $nombre) { ?>
        <tr>
            <td><?php echo $nombre['nombre'];?></td>
            <td><?php echo $nombre['duracion'];?></td>
            <td><?php echo $nombre['costo'];?></td>
             <td><?php echo $nombre['dias'];?></td>
           
            <td><a href="CrudCursos.php?editarC=<?php echo $nombre['_id'];?>" class="edit_btn" >Modificar</a></td>
            <td><a href="registros_cursos.php?del=<?php echo $nombre['_id'];?>" class="delete_btn">Eliminar</a></td>
        </tr>
        <?php }}?>
    </table>
     <?php if(isset($_SESSION['mensaje'])){
    ?>
    <div class="mensaje">
        <?php
        $mensaje=$_SESSION['mensaje'];
        echo $mensaje;
    ?>
    </div>
    <?php } ?>
  </body>
  </html>