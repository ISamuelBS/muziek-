 <?php include('php_code_cursos.php');?>
<?php 
if (isset($_GET['editarC'])) {
    $id_cliente = $_GET['editarC'];
    $update = true;
    $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_cursos = $mongo -> selectCollection("muziek","Cursos");
    $id = $_GET['editarC'];
    $condicion = array('_id' => new MongoId($id));
    if($c_cursos->count($condicion)==1){

        $row=$c_cursos->findOne($condicion);
        $nombre_c = $row['nombre'];
        $duracion = $row['duracion'];
        $costo = $row['costo'];
        $dias =implode(",", $row['días']);
        $_SESSION['mensaje'] = "Editando curso";
    }
} 
if(isset($_GET['eliminar'])){
   $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_cursos = $mongo -> selectCollection("muziek","Cursos");
        $id = $_GET['eliminar'];
        $condicion = array('_id' => new MongoId($id));
        $c_cursos->remove($condicion);
        $_SESSION['mensaje']="Registro eliminado";
}
 ?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/vnd.microsoft.icon" href="../images/Muziek-LOGO.ico" sizes="16x16 24x24 36x36 48x48">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300">
    <link rel="stylesheet" href="../css/font.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>CherryCode: Muziek</title>
  </head>
  <body>

    <input type="checkbox" id="btn-nav" class="checkbox">
    <header>
      <div class="header-container">
        <img class="header-logo" src="../images/MuziekONE.png"> 
        <label for="btn-nav" class="btn-label">
          <div class="header-button"></div>
        </label>
      </div>
    </header>
    
    <nav class="menu">
      <ul>
       <li><a href="index.php"></a></li>
        <li><a href="../alumnos/CrudAlumnos.php">Alumnos</a></li>
        <li><a href="../cursos/CrudCursos.php">Cursos</a></li>
        <li><a href="../maestros/CrudMaestros.php">Maestros</a></li>
        <li><a href="../Nosotros/Nosotros.php">Acerca de Nosotros</a></li>
      </ul>           
    </nav>
    <br><br><br><br><br><br><br>
    <?php if(isset($_SESSION['mensaje'])){
    ?>
    <div class="mensaje">
        <?php
        $mensaje=$_SESSION['mensaje'];
        echo $mensaje;
    ?>
    </div>
    <?php } ?>
     <form method="post" action="CrudCursos.php">
        <input type="hidden" name="id_curso" value="<?php echo$nombre['_id']?>">
        <div class="input-group">
            <label>Nombre Curso</label>
            <input type="text" name="nombre_c" value="<?php echo$nombre_c?>" required>
        </div>
        <div class="input-group">
            <label>Duración</label>
          <input type="text" name="duracion" value="<?php echo$duracion?>" required>
        </div>
         <div class="input-group">
            <label>Costo</label>
            <input type="text" name="costo" value="<?php echo$costo?>" required>
        </div>
       
        <div class="input-group">
            <label>Días</label>
            <input type="text" name="dias" value="<?php echo$dias?>" required>
        </div>
      
     <div class="input-group">
             <?php  if ($update == true): ?>
            <button class="btn" type="submit" name="actualizar">Actualizar</button>
            <?php else: ?>
            <button class="btn" type="submit" name="guardar">Registrar</button>
        <?php endif ?>
      

    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <?php $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_cursos = $mongo -> selectCollection("muziek","Cursos");

    if($c_cursos->count()==0){
        ?>
    <div class="vacio">Sin registros</div>
    <?php
    }else{
        ?>
    <table border="2">
        <thead>
            <tr>
            <th>Nombre</th>
            <th>Duración</th>
            <th>Costo</th>
            <th>Días</th>
            <th colspan="2">Acción</th>
            </tr>
        </thead>
        <?php  $row=$c_cursos->find();
        foreach ($row as $nombre) { 

          ?>

        <tr>
            <td><?php echo $nombre['nombre'];?></td>
            <td><?php echo $nombre['duracion'];?></td>
            <td><?php echo $nombre['costo'];?></td>
             <td><?php echo implode(",", $nombre['días']);?></td>
           
            <td><a href="CrudCursos.php?editarC=<?php echo $nombre['_id'];?>" class="edit_btn" >Modificar</a></td>
            <td><a href="CrudCursos.php?eliminar=<?php echo $nombre['_id'];?>" class="delete_btn">Eliminar</a></td>
        </tr>
        <?php }}?>
    </table>
    
    </form>

    
  </body>
</html>