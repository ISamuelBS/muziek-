<?php

$id="";
$nombre="";
$apellido="";
$sexo="";
$especialidad="";
$update=false;
session_start();
if (isset($_POST['guardar'])) {
    $mongo= new Mongo();
    $db= $mongo->selectDB("muziek");
    $c_maestros=$mongo->selectCollection("muziek","Maestros");

    $id=$_POST["id"];
    $nombre=$_POST["nombre"];
    $apellido=$_POST["apellido"];
    $sexo=$_POST["sexo"];
    $especialidad=$_POST["especialidad"];

    $nuevoMaestro= array(
        "id"=>$id,
        "nombre" => $nombre,
        "apellido" => $apellido,
        "genero" => $sexo,
        "especialidad" => $especialidad
    );
    $c_maestros->insert($nuevoMaestro);
    $_SESSION['mensaje'] = "Registro Agregado";
    header('location: CrudMaestros.php');
}

if
    (isset($_POST['update'])){
    $mongo=new Mongo();
        $db=$mongo->selectDB("muziek");
        $c_maestros = $mongo -> selectCollection("muziek","Maestros");
        $nombre=$_POST['nombre'];
        $apellido=$_POST['apellido'];
        $sexo=$_POST['sexo'];
        $especialidad=$_POST['especialidad'];
        $id=$_POST['id'];
        $condicion = array('_id' => new MongoID($id));
        $modi = array('nombre' => $nombre,
            'apellido'=>$apellido,
            'genero'=>$sexo,
            'especialidad'=>$especialidad
         );
        $c_maestros->update($condicion, $modi);

    $_SESSION['mensaje'] = "Registro Actualizado";
    header('location: CrudMaestros.php');
}
?>